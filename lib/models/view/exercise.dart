// Copyright (C) 2019 Miroslav Mazel
//
// This file is part of Feeel.
//
// Feeel is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version. As an additional permission under
// section 7, you are allowed to distribute the software through an app
// store, even if that store has restrictive terms and conditions that
// are incompatible with the AGPL, provided that the source is also
// available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// Feeel is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Feeel.  If not, see <http://www.gnu.org/licenses/>.

import 'package:feeel/enums/exercise_type.dart';

// import 'exercise_step.dart';

class Exercise {
  final int dbId;
  final String name;
  final String description;
  final bool flipped;
  final bool animated;
  final ExerciseType type;
  final String? imageSlug;
  final String? descriptionLicense;
  final String? imageLicense;
  // final List<ExerciseStep>? steps;

  Exercise({
    required this.dbId,
    required this.name,
    required this.description,
    required this.flipped,
    required this.type,
    required this.animated,
    this.imageSlug,
    this.imageLicense,
    this.descriptionLicense,
    // this.steps
  });
}
